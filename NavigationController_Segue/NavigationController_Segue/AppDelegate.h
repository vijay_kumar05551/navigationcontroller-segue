//
//  AppDelegate.h
//  NavigationController_Segue
//
//  Created by OSX on 16/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

